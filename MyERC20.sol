pragma solidity ^0.4.26;

contract ERC20 {
    
    string public name;
    string public symbol;
    uint256 public totalSupply;
    uint8 public constant decimals = 18;  

    event Transfer(address indexed from, address indexed to, uint tokens);
    event Approval(address indexed tokenOwner, address indexed spender, uint tokens);

    mapping(address => uint256) balances;
    mapping(address => mapping (address => uint256)) allowed;
    
    constructor( uint256 initialSupply, string tokenName, string tokenSymbol) public {
        name = tokenName;                                       
        symbol = tokenSymbol; 
	    totalSupply = initialSupply * 10 ** uint256(decimals);
	    balances[this] = totalSupply;
    }  

    function totalSupply() public view returns (uint256) {
	    return totalSupply;
    }
    
    function balanceOf(address tokenOwner) public view returns (uint) {
        return balances[tokenOwner];
    }

    function transfer(address receiver, uint numTokens) public returns (bool) {
        require(numTokens <= balances[msg.sender]);
        balances[msg.sender] = balances[msg.sender] - numTokens;
        emit Transfer(msg.sender, receiver, numTokens);
        return true;
    }

    function approve(address delegate, uint numTokens) public returns (bool) {
        allowed[msg.sender][delegate] = numTokens;
        emit Approval(msg.sender, delegate, numTokens);
        return true;
    }

    function allowance(address owner, address delegate) public view returns (uint) {
        return allowed[owner][delegate];
    }

    function transferFrom(address owner, address buyer, uint numTokens) public returns (bool) {
        require(numTokens <= balances[owner]);    
        require(numTokens <= allowed[owner][msg.sender]);
    
        balances[owner] = balances[owner] - numTokens;
        allowed[owner][msg.sender] = allowed[owner][msg.sender] - (numTokens);
        balances[buyer] = balances[buyer] + numTokens;
        emit Transfer(owner, buyer, numTokens);
        return true;
    }
}

contract MyERC20 is ERC20 {
    uint256 public unitsOneEthCanBuy;
    constructor( uint256 initialSupply, string tokenName, string tokenSymbol) ERC20 (initialSupply, tokenName, tokenSymbol) public {
        unitsOneEthCanBuy = 10;
    } 
    
    // Let's code here

}



